
#[derive(Debug, PartialEq)]
pub enum Token {
    Illegal,
    EndOfFile,

    Identifier(String),
    Integer(i32),

    Assign,
    Equal,
    NotEqual,

    LowerThan,
    GreaterThan,

    Asterisk,
    Slash,
    Plus,
    Minus,
    Bang,
    Comma,
    Semicolon,

    LeftParen,
    RightParen,
    LeftBrace,
    RightBrace,

    Function,
    Let,
    If,
    Else,
    True,
    False,
    Return,
}


pub fn lookup_identifier(identifier: &str) -> Token {
    match identifier {
        "fn" => Token::Function,
        "let" => Token::Let,
        "if" => Token::If,
        "else" => Token::Else,
        "true" => Token::True,
        "false" => Token::False,
        "return" => Token::Return,
        _ => Token::Identifier(identifier.to_string()),
    }
}
