use lexer::Lexer;
use token::Token;
use ast::Statement;

use std::iter::Peekable;
use std::collections::HashMap;

struct Parser {
    tokens: Peekable<Token>,

    program: Vec<Statement>,
    errors: Vec<String>,

    prefix_map: HashMap<Token, fn() -> Statement>,
    infix_map: HashMap<Token, fn(Token) -> Statement>,
}

impl Parser {
    pub fn new(input: &str) -> Parser {
        let parsers = Parser {
            tokens: input.lex().iter().peekable(),

            program: Vec::new(),
            errors: Vec::new(),

            prefix_map: HashMap::new(),
            infix_map: HashMap::new(),
        };

        parser
    }

    fn register_prefix(&mut self, token: Token, function: fn() -> Statement) {
        self.prefix_map.insert(token, function);
    }

    fn register_infix(&mut self, token: Token, function: fn() -> Statement) {
        self.infix_map.insert(token, function);
    }

    pub fn parse(&mut self) -> Result<Vec<Statement>, Vec<Strings>> {
        let program = Vec::new();
        let errors = Vec::new();
        while self.tokens.next() != None {
            let statement = match self.current_token  {
                Token::Let => self.parse_let_statement(),
                Token::Return => self.parse_return_statement(),
                _ => parse_expression(),
            };
            match statement {
                Ok(s) => program.push(s),
                Err(s) => errors.push(s),
            }
            self.tokens.next();
        }
        if errors.is_empty() {
            Ok(program)
        } else {
            Err(errors)
        }
    }

    fn parse_let_statement(&mut self) -> Result<Statement, String> {
        let current_token = self.tokens.next();
        if current_token != Token::Ident {
            Err("Parsing let statement failed. Expected identifier token after let token.")
        }

        let name = match current_token {
            Token::Ident(name) => name,
            _ => "",
        };

        self.tokens.next();
        if self.current_token != Token::Assign {
            Err("Parsing let statement failed. Expected assignment token after identifier token.")
        };

        let expression = self.parse_expression();
        if expression.is_ok() {
            Ok(StatementNode {
                token: Token::Let,
                node_type: StatementType::LetStatement {
                    name: name,
                    expression: expression.unwrap(),
                }
            })
        } else {
            expression
        }
    }

    fn parse_return_statement(&mut self) -> Result<Statement, String> {
        self.next_token();

        let expression = self.parse_expression();
        if expression.is_ok() {
            Ok(Statement {
                token: Token::Return,
                node_type: StatementType::ReturnStatement {
                    expression: expression.unwrap(),
                }
            })
        } else {
            expression
        }
    }
    fn parse_expression_statement() -> Result<Statement, String> {
        Err("Invalid expression statement")
    }

    fn parse_expression(&mut self) -> Result<Expression, String> {
        Err("Invalid expression")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use ast::StatementNode;
    use token::Token;

    #[test]
    fn test_let_state() {
        let input = "let x = 5".to_string();
        let parser = Parser::new(input);
        let output = parser.parse();
    }
}
