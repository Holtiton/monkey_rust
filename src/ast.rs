use token::Token;

#[derive(Debug)]
pub enum Statement {
    LetStatement {
        name: &str,
        expression: Expression,
    },
    ReturnStatement(Expression),
    BlockStatement(Vec<Expression>),
    ExpressionStatement(Expression),
}

#[derive(Debug)]
pub enum Expression {
    Identifier {
        value: &str
    },
    Number {
        value: Int
    },
    Binary {
        op: Symbol,
        left: Box<Experssion>,
        right: Box<Experssion>,
    },
    Unary {
        op: Token,
        right: Box<Expression>
    },
    If {
        condition: Box<Expression>,
        consequence: Box<Expression>,
        alternative: Option<Box<Expression>>,
    },
    Function {
        parameters: Vec<Box<Experssion>>,
        body: StatementType,
    },
    Call {
        function: Box<Expression>,
        arguments: Vec<Box<Expression>>,
    },
}

#[derive(Debug, PartialEq)]
pub enum Symbol {
    Add,
    Subtract,
    Multiply,
    Divide,
}
