use token;
use token::Token;

use std::str::Chars;
use std::iter::Peekable;

pub fn lex(input: &str) -> Vec<Token> {
    let mut input = input.chars().peekable();

    let mut tokens = Vec::new();

    while let Some(ch) = input.next() {
        skip_whitespace(&mut input);
        match ch {
            '*' => tokens.push(Token::Asterisk),
            '/' => tokens.push(Token::Slash),
            '+' => tokens.push(Token::Plus),
            '-' => tokens.push(Token::Minus),
            '(' => tokens.push(Token::LeftParen),
            ')' => tokens.push(Token::RightParen),
            '{' => tokens.push(Token::LeftBrace),
            '}' => tokens.push(Token::RightBrace),
            ',' => tokens.push(Token::Comma),
            '<' => tokens.push(Token::LowerThan),
            '>' => tokens.push(Token::GreaterThan),
            ';' => tokens.push(Token::Semicolon),
            '!' => match input.peek() {
                Some(&'=') => { input.next(); tokens.push(Token::NotEqual) },
                Some(_) => tokens.push(Token::Bang),
                None => tokens.push(Token::Illegal),
            },
            '=' => match input.peek() {
                Some(&'=') => { input.next(); tokens.push(Token::Equal) },
                Some(_) => tokens.push(Token::Assign),
                None => tokens.push(Token::Illegal),
            },
            ' ' => (),
            ch @ 'A'...'Z' |
            ch @ 'a'...'z' => tokens.push(read_identifier(&mut input, ch)),
            num @ '0'...'9' => tokens.push(read_number(&mut input, num)),
            _ => tokens.push(Token::Illegal),
        }
    }
    tokens.push(Token::EndOfFile);
    tokens
}

fn read_identifier(input: &mut Peekable<Chars>, first: char) -> Token {
    let mut identifier = String::new();
    identifier.push(first);
    while let Some(&ch) = input.peek() {
        if !ch.is_alphabetic() && !ch.is_numeric() && ch != '_' {
            break;
        } else {
            // TODO: proper error checking here
            identifier.push(input.next().unwrap());
        }
    }

    token::lookup_identifier(identifier.as_ref())
}

fn read_number(input: &mut Peekable<Chars>, first: char) -> Token {
    let mut number = String::new();
    number.push(first);
    while let Some(&ch) = input.peek() {
        if !ch.is_numeric() {
            break;
        } else {
            // TODO: proper error checking here
            number.push(input.next().unwrap());
        }
    }

    // TODO: I should probably do something here
    Token::Integer(number.parse::<i32>().unwrap())
}

fn skip_whitespace(input: &mut Peekable<Chars>) {
    while let Some(&ch) = input.peek() {
        if !ch.is_whitespace() {
            break;
        } else {
            input.next();
        }
    }
}



#[cfg(test)]
mod tests {
    use super::*;
    use token::Token;

    #[test]
    fn lexer_test() {

        let input = "let five1 = 5;
let ten_10 = 10;
let add = fn(x, y) {
  x + y;
};
let result = add(five, ten);
!-/*5;
5 < 10 > 5;
if (5 < 10) {
    return true;
} else {
    return false;
}
10 == 10;
10 != 9;".to_string();

        let tests = vec![
            Token::Let,
            Token::Identifier("five1".to_string()),
            Token::Assign,
            Token::Integer(5),
            Token::Semicolon,
            Token::Let,
            Token::Identifier("ten_10".to_string()),
            Token::Assign,
            Token::Integer(10),
            Token::Semicolon,
            Token::Let,
            Token::Identifier("add".to_string()),
            Token::Assign,
            Token::Function,
            Token::LeftParen,
            Token::Identifier("x".to_string()),
            Token::Comma,
            Token::Identifier("y".to_string()),
            Token::RightParen,
            Token::LeftBrace,
            Token::Identifier("x".to_string()),
            Token::Plus,
            Token::Identifier("y".to_string()),
            Token::Semicolon,
            Token::RightBrace,
            Token::Semicolon,
            Token::Let,
            Token::Identifier("result".to_string()),
            Token::Assign,
            Token::Identifier("add".to_string()),
            Token::LeftParen,
            Token::Identifier("five".to_string()),
            Token::Comma,
            Token::Identifier("ten".to_string()),
            Token::RightParen,
            Token::Semicolon,
            Token::Bang,
            Token::Minus,
            Token::Slash,
            Token::Asterisk,
            Token::Integer(5),
            Token::Semicolon,
            Token::Integer(5),
            Token::LowerThan,
            Token::Integer(10),
            Token::GreaterThan,
            Token::Integer(5),
            Token::Semicolon,
            Token::If,
            Token::LeftParen,
            Token::Integer(5),
            Token::LowerThan,
            Token::Integer(10),
            Token::RightParen,
            Token::LeftBrace,
            Token::Return,
            Token::True,
            Token::Semicolon,
            Token::RightBrace,
            Token::Else,
            Token::LeftBrace,
            Token::Return,
            Token::False,
            Token::Semicolon,
            Token::RightBrace,
            Token::Integer(10),
            Token::Equal,
            Token::Integer(10),
            Token::Semicolon,
            Token::Integer(10),
            Token::NotEqual,
            Token::Integer(9),
            Token::Semicolon,
            Token::EndOfFile
        ];

        let tokens = lex(&input);
        assert_eq!(tokens, tests);
    }
}
